#ifndef USER_H
#define USER_H

#include <string>

class User
{
    public:

        /**
         *  Constructor.
         */
        User(std::string name, int age);

        /**
         *  Destructor.
         */
        virtual ~User();

        std::string getName() { return this->name; }
        int getAge() { return this->age; }

    private:
        std::string name;
        int         age;
};

#endif
