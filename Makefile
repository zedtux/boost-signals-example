CC=g++
CFLAGS=-pedantic -Wall -c -W
LDFLAGS=-lboost_signals
OBJ=user.o class_a.o class_b.o main.o
INSTALL=/usr/bin/install -c
BINDIR=/usr/local/bin
EXEC=signal_demo

all: $(EXEC) $(EXECTEST)

#
# Dev Tools
#

# Clean project
clean:
	rm -rf *.o *.log *~ *.xml

mrproper: clean
	rm -rf $(EXEC) $(EXECTEST)

#
# Compile
#

signal_demo: $(OBJ)
	$(CC) -o $(EXEC) $(OBJ) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<