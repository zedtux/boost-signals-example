#include "class_a.h"

ClassA::ClassA()
{
    std::cout << "ClassA::ClassA" << std::endl;
}

ClassA::~ClassA()
{
    std::cout << "ClassA::~ClassA" << std::endl;
}

void ClassA::create_new_user(std::string name, int age)
{
    std::cout << "ClassA::create_new_user" << std::endl;

    // Instanciate a new User
    User new_user(name, age);

    // Pass a reference of this user to all subscribed objects
    user_created(&new_user);
}

boost::signals2::connection ClassA::connect(const signalUserCreatedType &slot)
{
    std::cout << "ClassA::connect" << std::endl;

    return user_created.connect(slot);
}

ClassA::signalUserCreated ClassA::user_created;
