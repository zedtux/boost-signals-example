<p align="center">
  <img src="https://raw.github.com/zedtux/gpair/master/media/developpeur_breton_logo.png" alt="Developpeur Breton" title="Developpeur Breton"/>
</p>

[![endorse](http://api.coderwall.com/zedtux/endorsecount.png)](http://coderwall.com/zedtux)

# Boost signals example

This repositry contains the sources of a simple example to show how to use [Boost::bind](http://www.boost.org/libs/bind/).

The article is available at [http://blog.zedroot.org/2013/01/boost-boostbind-calling-multiple-times-the-object-destructor/](http://blog.zedroot.org/2013/01/boost-boostbind-calling-multiple-times-the-object-destructor/)

## Issues

If you find a bug please [open a new issue](/zedtux/boost-signals-example/issues/new).
