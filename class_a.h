#ifndef CLASS_A_H
#define CLASS_A_H

#include <boost/signals2.hpp>
#include "user.h"

class ClassA
{
    public:
        typedef boost::signals2::signal<void(User*)> signalUserCreated;
        typedef signalUserCreated::slot_type signalUserCreatedType;

        /**
         *  Constructor.
         */
        ClassA();

        /**
         *  Destructor.
         */
        virtual ~ClassA();

        /**
         *  \brief create_new_user
         *
         *  Create a new user and fire the signal user_created
         *  in order to notify all subscribed objects.
         */
        void create_new_user(std::string name, int age);

        /**
         *  \brief connect
         *
         *  Subscribe given subscriber to the signalUserCreated signal
         *
         *  \param subscriber Remote function address
         *
         *  \return connected instance of boost::signals::connection
         */
        static boost::signals2::connection  connect(const signalUserCreatedType &slot);

    private:
        static signalUserCreated user_created;
};

#endif
