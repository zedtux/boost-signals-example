#include "class_a.h"
#include "class_b.h"

int main(int argc, char **argv)
{
	ClassA class_a;
	ClassB class_b;

	class_a.connect(boost::bind(&ClassB::on_new_user, &class_b, _1));

	class_a.create_new_user("Guillaume", 29);

	return 0;
}