#ifndef CLASS_B_H
#define CLASS_B_H

#include <boost/signals2.hpp>
#include <sstream>
#include "user.h"

class ClassB
{
    public:

        /**
         *  Constructor.
         */
        ClassB();

        /**
         *  Destructor.
         */
        virtual ~ClassB();

        /**
         *  \brief on_new_user
         *
         *  Will be fired by ClassA whenever a new user is created
         */
        void on_new_user(User * new_user) const;
};

#endif
