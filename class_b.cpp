#include "class_b.h"

ClassB::ClassB()
{
    std::cout << "ClassB::ClassB" << std::endl;
}

ClassB::~ClassB()
{
    std::cout << "ClassB::~ClassB" << std::endl;
}

void ClassB::on_new_user(User * new_user) const
{
    std::cout << "ClassB::on_new_user" << std::endl;

	std::stringstream ss;
    ss << "A new user named " << new_user->getName();
    ss << " who is " << new_user->getAge();
    ss << " has been created in ClassA.";
    std::cout << ss.str() << std::endl;
}